package com.bill.atl.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.bill.atl.ui.MyInput;
import com.bill.atl.ui.ObpButton;
import com.bill.atl.ui.ObpText;

public class CreateIssueFragment extends BasePageObject {

    public static final String ID_SUMMARY_INPUT = "summary";

    public static final String XPATH_CREATE_SUBMIT_BUTTON = ".//input[@id='create-issue-submit' and @value='Create' and @type='submit']";

    public static final String XPATH_PROJECT_INPUT = ".//div[@id='project-single-select']/input[@id='project-field']";

    public static final String XPATH_TITLE_TEXT = ".//h2[@title='Create Issue' and text() = 'Create Issue']";

    public static CreateIssueFragment waitforInstance(WebDriver driver) {
        if (null == waitfor("wait for create-issue-form title", driver,
                By.xpath(XPATH_TITLE_TEXT))
                || null == waitfor("wait for create-issue-form create-submit",
                        driver, By.xpath(XPATH_CREATE_SUBMIT_BUTTON))) {
            return null;
        }
        return new CreateIssueFragment(driver);
    }

    /** mandatory field createSubmitButton */
    @FindBy(xpath = XPATH_CREATE_SUBMIT_BUTTON)
    private WebElement createSubmitButton;

    /** mandatory field projectInput */
    @FindBy(xpath = XPATH_PROJECT_INPUT)
    private WebElement projectInput;

    /** mandatory field summaryInput */
    @FindBy(id = ID_SUMMARY_INPUT)
    private WebElement summaryInput;

    /** mandatory field titleText */
    @FindBy(xpath = XPATH_TITLE_TEXT)
    private WebElement titleText;

    public CreateIssueFragment(WebDriver webdriver) {
        super(webdriver);
    }

    public boolean clickCreateSubmitButton() {
        return clickOn(createSubmitButton);
    }

    public ObpButton getCreateSubmitButton() {
        return new ObpButton(createSubmitButton);
    }

    public MyInput getProjectInput() {
        return new MyInput(projectInput);
    }

    public MyInput getSummaryInput() {
        return new MyInput(summaryInput);
    }

    public ObpText getTitleText() {
        return new ObpText(titleText);
    }

    public boolean hasCreateSubmitButton() {
        createSubmitButton = lookfor(driver,
                By.xpath(XPATH_CREATE_SUBMIT_BUTTON));
        return isVisible(createSubmitButton);
    }

    public boolean hasProjectInput() {
        projectInput = lookfor(driver, By.xpath(XPATH_PROJECT_INPUT));
        return isVisible(projectInput);
    }

    public boolean hasSummaryInput() {
        summaryInput = lookfor(driver, By.id(ID_SUMMARY_INPUT));
        return isVisible(summaryInput);
    }

    public boolean hasTitleText() {
        titleText = lookfor(driver, By.xpath(XPATH_TITLE_TEXT));
        return isVisible(titleText);
    }

    public boolean inputAtProjectInput(String text) {
        return inputAt(projectInput, text);
    }

    public boolean inputAtSummaryInput(String text) {
        return inputAt(summaryInput, text);
    }

    public boolean touchProjectInput() {
        return clickOn(projectInput);
    }

    public boolean touchSummaryInput() {
        return clickOn(summaryInput);
    }

    public boolean touchTitleText() {
        return clickOn(titleText);
    }

}
