package com.bill.atl.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bill.atl.ui.MyInput;
import com.bill.atl.ui.ObpButton;

public class LoginPage extends BasePageObject {
    public static final String ID_SIGN_IN_GOOGLE_BUTTON = "google-signin-button";

    public static final String XPATH_SIGN_IN_BUTTON = ".//input[@type='submit' and @id='login-submit']";

    public static final String XPATH_USERNAME_INPUT = ".//input[@id='username' and @name='username' and @type='email']";

    public static final String XPATH_USERPASS_INPUT = ".//input[@id='password' and @type='password']";

    public static LoginPage waitforInstance(WebDriver driver) {
        if (null == waitfor("wf for username input", driver,
                By.xpath(XPATH_USERNAME_INPUT))) {
            return null;
        }
        return new LoginPage(driver);
    }

    protected WebDriver driver;

    /** mandatory field signInButton */
    @FindBy(xpath = XPATH_SIGN_IN_BUTTON)
    private WebElement signInButton;

    /** mandatory field signInGoogleButton */
    @FindBy(id = ID_SIGN_IN_GOOGLE_BUTTON)
    private WebElement signInGoogleButton;

    /** mandatory field usernameInput */
    @FindBy(xpath = XPATH_USERNAME_INPUT)
    private WebElement usernameInput;

    /** mandatory field userpassInput */
    @FindBy(xpath = XPATH_USERPASS_INPUT)
    private WebElement userpassInput;

    public LoginPage(WebDriver webdriver) {
        super(webdriver);
        // put page factory init here only indicate attention has paid to .pdf
        // it's a debate whether should add this due to efficiency consideration
        PageFactory.initElements(driver, this);

    }

    public PrivateProjectPage clickSignInButton() {
        clickOn(signInButton);
        return PrivateProjectPage.waitforInstance(driver);
    }

    public GoogleAuthPage clickSignInGoogleButton() {
        clickOn(signInGoogleButton);
        return GoogleAuthPage.waitforInstance(driver);
    }

    public ObpButton getSignInButton() {
        return new ObpButton(signInButton);
    }

    public ObpButton getSignInGoogleButton() {
        return new ObpButton(signInGoogleButton);
    }

    public MyInput getUsernameInput() {
        return new MyInput(usernameInput);
    }

    public MyInput getUserpassInput() {
        return new MyInput(userpassInput);
    }

    public boolean hasSignInButton() {
        signInButton = lookfor(driver, By.xpath(XPATH_SIGN_IN_BUTTON));
        return isVisible(signInButton);
    }

    public boolean hasSignInGoogleButton() {
        signInGoogleButton = lookfor(driver, By.id(ID_SIGN_IN_GOOGLE_BUTTON));
        return isVisible(signInGoogleButton);
    }

    public boolean hasUsernameInput() {
        usernameInput = lookfor(driver, By.xpath(XPATH_USERNAME_INPUT));
        return isVisible(usernameInput);
    }

    public boolean hasUserpassInput() {
        userpassInput = lookfor(driver, By.xpath(XPATH_USERPASS_INPUT));
        return isVisible(userpassInput);
    }

    public boolean inputAtUsernameInput(String text) {
        return inputAt(usernameInput, text);
    }

    public boolean inputAtUserpassInput(String text) {
        return inputAt(userpassInput, text);
    }

    public boolean touchUsernameInput() {
        return clickOn(usernameInput);
    }

    public boolean touchUserpassInput() {
        return clickOn(userpassInput);
    }

}
