package com.bill.atl.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.bill.atl.bo.IssueCreatedPopup;
import com.bill.atl.ui.ObpLink;

public class PrivateProjectPage extends PublicProjectPage {

    /** mandatory field createIssueLink */
    @FindBy(xpath = XPATH_CREATE_ISSUE_LINK)
    private WebElement createIssueLink;

    public PrivateProjectPage(WebDriver webdriver) {
        super(webdriver);
    }

    public CreateIssueFragment clickCreateIssueLink() {
        clickOn(createIssueLink);
        return CreateIssueFragment.waitforInstance(driver);
    }

    public ObpLink getCreateIssueLink() {
        return new ObpLink(createIssueLink);
    }

    public boolean hasCreateIssueLink() {
        createIssueLink = lookfor(driver, By.xpath(XPATH_CREATE_ISSUE_LINK));
        return isVisible(createIssueLink);
    }

    public IssueCreatedPopup obtainIssueCreatedPopup() {
        // TODO Auto-generated method stub
        return null;
    }

}
