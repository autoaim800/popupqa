package com.bill.atl.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bill.atl.ui.ObpLink;
import com.bill.atl.ui.ObpText;

public class PublicProjectPage extends BasePageObject {
    public static final String XPATH_CREATE_ISSUE_LINK = ".//li[@id='create-menu']/a[@id='create_link']";

    public static final String XPATH_LOG_IN_LINK = ".//li[@id='user-options']/a[text() = 'Log In']";

    public static final String XPATH_PROJECT_KEY_TEXT = ".//span[@id='pd-key']";

    public static final String XPATH_PROJECT_NAME_TEXT = ".//div/h1[text() = 'A Test Project']";

    public static PrivateProjectPage waitforInstance(WebDriver webDriver) {
        if (null == waitfor("wait for project-home-page title", webDriver,
                By.xpath(XPATH_PROJECT_KEY_TEXT))
                || null == waitfor("wait for project-home-page key", webDriver,
                        By.xpath(XPATH_PROJECT_NAME_TEXT))) {
            return null;
        }
        return new PrivateProjectPage(webDriver);
    }

    /** mandatory field logInLink */
    @FindBy(xpath = XPATH_LOG_IN_LINK)
    private WebElement logInLink;

    /** mandatory field projectKeyText */
    @FindBy(xpath = XPATH_PROJECT_KEY_TEXT)
    private WebElement projectKeyText;

    /** mandatory field projectNameText */
    @FindBy(xpath = XPATH_PROJECT_NAME_TEXT)
    private WebElement projectNameText;

    public PublicProjectPage(WebDriver webdriver) {
        super(webdriver);
        // put page factory init here only indicate attention has paid to .pdf
        // it's a debate whether should add this due to efficiency consideration
        PageFactory.initElements(driver, this);
    }

    public LoginPage clickLogInLink() {
        clickOn(logInLink);
        return LoginPage.waitforInstance(driver);
    }

    public ObpLink getLogInLink() {
        return new ObpLink(logInLink);
    }

    public ObpText getProjectKeyText() {
        return new ObpText(projectKeyText);
    }

    public ObpText getProjectNameText() {
        return new ObpText(projectNameText);
    }

    public boolean hasLogInLink() {
        logInLink = lookfor(driver, By.xpath(XPATH_LOG_IN_LINK));
        return isVisible(logInLink);
    }

    public boolean hasProjectKeyText() {
        projectKeyText = lookfor(driver, By.xpath(XPATH_PROJECT_KEY_TEXT));
        return isVisible(projectKeyText);
    }

    public boolean hasProjectNameText() {
        projectNameText = lookfor(driver, By.xpath(XPATH_PROJECT_NAME_TEXT));
        return isVisible(projectNameText);
    }

    public boolean touchProjectKeyText() {
        return clickOn(projectKeyText);
    }

    public boolean touchProjectNameText() {
        return clickOn(projectNameText);
    }
}
