package com.bill.atl.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyPage {
    /**
     * wait for certain web-element with given by using given web-driver
     * 
     * @param msg
     *            a string of message of what/why waiting
     * @param driver
     *            an object of active web-driver
     * @param by
     *            an object of search criteria
     * @return an object of web-element, null for no found
     */
    public static WebElement waitfor(String msg, WebDriver driver, By by) {
        // TODO Auto-generated method stub
        return null;
    }

    protected boolean clickOn(WebElement el) {
        el.click();
        return waitForAnimation();
    }

    protected boolean inputAt(WebElement el, String text) {
        // TODO Auto-generated method stub
        return false;
    }

    protected boolean isVisible(WebElement el) {
        return null != el && el.isDisplayed();
    }

    protected WebElement lookfor(WebDriver driver, By by) {
        return driver.findElement(by);
    }

    private boolean waitForAnimation() {
        // TODO Auto-generated method stub
        return false;
    }
}
