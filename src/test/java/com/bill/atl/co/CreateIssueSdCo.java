package com.bill.atl.co;

import static org.junit.Assert.assertNotNull;

import com.bill.atl.bo.IssueFormBo;
import com.bill.atl.bo.LoginBo;
import com.bill.atl.bo.LogoutBo;
import com.bill.atl.bo.PrivateProjectBo;
import com.bill.atl.bo.PublicProjectBo;
import com.bill.atl.pojo.IssueDo;

/**
 * single driver instance of create issue case-object - co, co controls driver
 * and properties
 */
public class CreateIssueSdCo extends SingleDriverCo {

    private PrivateProjectBo priBoardBo;

    private PublicProjectBo pubBoardBo;

    public CreateIssueSdCo() {
        super();
        assertNotNull(driver);

        get(props.getProperty("tst-url"));

        pubBoardBo = new PublicProjectBo(driver);
        assertNotNull(pubBoardBo);
    }

    public void createIssue(IssueDo issue) {
        assertNotNull(issue);
        assertNotNull(priBoardBo);

        IssueFormBo formBo = priBoardBo.displayIssueForm();
        assertNotNull(formBo);

        priBoardBo = formBo.fillInSubmit(issue);
        assertNotNull(priBoardBo);

        issue = priBoardBo.obtainNewlyCreatedIssueInfo();
        assertNotNull(issue);

        // stop verifying here

    }

    /**
     * close and quit current driver
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }

    public void login() {
        LoginBo lb = pubBoardBo.navigateToLoginPage();

        priBoardBo = lb.loginAs(props.getProperty("username"),
                props.getProperty("userpass"));
        assertNotNull(priBoardBo);
    }

    public void logout() {
        LogoutBo lb = new LogoutBo(priBoardBo.getDriver());
        pubBoardBo = lb.logout();
    }
}
