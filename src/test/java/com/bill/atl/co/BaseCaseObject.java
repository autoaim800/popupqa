package com.bill.atl.co;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

/**
 * case object invokes business objects to serve the test-case
 */
public class BaseCaseObject {

    public static final String DEFAULT_PROP_FILENAME = "tst.properties";

    public static Properties props;

    static {
        props = loadProperties(DEFAULT_PROP_FILENAME);
    }

    private static Properties loadProperties(String filePathName) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * obtain a driver from driver pool
     * 
     * @return an object of driver, null for unavailable
     */
    public static WebDriver obtainDriver() {
        // TODO Auto-generated method stub
        return null;
    }

}
