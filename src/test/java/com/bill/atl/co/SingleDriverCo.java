package com.bill.atl.co;

import org.openqa.selenium.WebDriver;

public class SingleDriverCo extends BaseCaseObject {
    protected WebDriver driver;

    public SingleDriverCo() {
        driver = obtainDriver();
    }

    public void get(String url) {
        driver.get(url);
    }

    public WebDriver getDriver() {
        return driver;
    }

}
