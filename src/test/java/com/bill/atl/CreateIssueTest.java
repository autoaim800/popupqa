package com.bill.atl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.bill.atl.co.CreateIssueSdCo;
import com.bill.atl.pojo.IssueDo;

public class CreateIssueTest {
    private CreateIssueSdCo co;

    @Before
    public void setUp() {
        co = new CreateIssueSdCo();
        co.login();
    }

    @After
    public void tearDown() {
        co.logout();
        co.destroy();
    }

    @Test
    public void testCreateIssuePostivie() {
        IssueDo issue = new IssueDo();
        // issue's setters

        // assertions are inside, bear with it
        co.createIssue(issue);
    }
}
