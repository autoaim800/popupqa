package com.bill.atl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.bill.atl.po.CreateIssueFragment;

public class AuthAccessTest extends MyTestCase {

    @Before
    public void setUp() {
        super.setUp();
        super.proceedToHomePageLoggedIn();
    }

    /**
     * preconditions
     * <ul>
     * <li>entry URL can be accessed anonymously</li>
     * <li>login link can be clicked</li>
     * <li>login page can be displayed</li>
     * <li>given credential is valid</li>
     * <li>logged in home page can be displayed</li>
     * </ul>
     */
    @Test
    public void test201CreateIssueForm() {
        CreateIssueFragment cif = hp.clickCreateIssueLink();
        assertNotNull(cif);

        assertTrue("no found project field", cif.hasProjectInput());
        assertTrue(cif.getProjectInput().isEditable());
        assertEquals("A Test Project (TST)", cif.getProjectInput().getValue());

        // a lot fields to be asserted
    }

}
