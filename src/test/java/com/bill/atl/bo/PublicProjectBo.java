package com.bill.atl.bo;

import static org.junit.Assert.assertNotNull;

import org.openqa.selenium.WebDriver;

import com.bill.atl.po.PrivateProjectPage;
import com.bill.atl.po.PublicProjectPage;

public class PublicProjectBo extends BaseBusinessObject {

    private PrivateProjectPage page;

    public PublicProjectBo(WebDriver webdriver) {
        super(webdriver);
        page = PublicProjectPage.waitforInstance(driver);
        assertNotNull(page);
    }

    public LoginBo navigateToLoginPage() {
        // TODO Auto-generated method stub
        return null;
    }

}
