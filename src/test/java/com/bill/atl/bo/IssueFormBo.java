package com.bill.atl.bo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.bill.atl.po.CreateIssueFragment;
import com.bill.atl.po.PrivateProjectPage;
import com.bill.atl.pojo.IssueDo;

public class IssueFormBo extends BaseBusinessObject {

    private CreateIssueFragment frag;

    public IssueFormBo(CreateIssueFragment cif) {
        super(cif.getDriver());
        frag = cif;
    }

    /**
     * fill in all issue fields and submit
     * 
     * @param issue
     *            an object of issue data object
     * @return a bo of private-project-bo, null for submission failure
     */
    public PrivateProjectBo fillInSubmit(IssueDo issue) {
        assertNotNull(issue);
        assertNotNull(frag);

        assertTrue(frag.hasProjectInput());
        assertTrue(frag.getProjectInput().isEditable());
        frag.inputAtProjectInput(issue.getProjectName());

        // verify a lot of fields here

        assertTrue(frag.hasCreateSubmitButton());
        assertTrue(frag.getCreateSubmitButton().isClickable());
        frag.clickCreateSubmitButton();

        PrivateProjectPage pri = PrivateProjectPage.waitforInstance(driver);
        assertNotNull(pri);

        return new PrivateProjectBo(pri);
    }

}
