package com.bill.atl.bo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;

import com.bill.atl.po.CreateIssueFragment;
import com.bill.atl.po.PrivateProjectPage;
import com.bill.atl.pojo.IssueDo;

public class PrivateProjectBo extends PublicProjectBo {

    private PrivateProjectPage page;

    public PrivateProjectBo(PrivateProjectPage pri) {
        super(pri.getDriver());
        page = pri;
        assertNotNull(page);
    }

    public PrivateProjectBo(WebDriver webdriver) {
        super(webdriver);
        page = PrivateProjectPage.waitforInstance(driver);
        assertNotNull(page);
    }

    /**
     * click the 'Create' link/button
     * 
     * @return an object of issue-form-bo
     */
    public IssueFormBo displayIssueForm() {
        assertTrue(page.hasCreateIssueLink());
        assertTrue(page.getCreateIssueLink().isClickable());
        page.clickCreateIssueLink();

        CreateIssueFragment cif = CreateIssueFragment.waitforInstance(driver);
        assertNotNull(cif);

        // all elements are to be verified later when inputting

        return new IssueFormBo(cif);
    }

    /**
     * retrieve newly create issue info by the popup
     * 
     * @return an object of issue
     */
    public IssueDo obtainNewlyCreatedIssueInfo() {
        IssueCreatedPopup popup = page.obtainIssueCreatedPopup();
        assertNotNull(popup);
        assertTrue(popup.hasDismissIcon());
        popup.clickDismissIcon();
        assertTrue(popup.hasIssueKey());
        IssueDo issue = new IssueDo(popup.getIssueKey());

        return issue;
    }

}
