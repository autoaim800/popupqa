package com.bill.atl.bo;

import org.openqa.selenium.WebDriver;

/**
 * business object invokes page-object to serve case object
 */
public class BaseBusinessObject {
    protected WebDriver driver;

    public BaseBusinessObject(WebDriver webdriver) {
        driver = webdriver;
    }

    public WebDriver getDriver() {
        return driver;
    }
}
