package com.bill.atl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import com.bill.atl.po.GoogleAuthPage;

public class PubAccessTest extends MyTestCase {

    @Before
    public void setUp() {
        super.setUp();
        super.proceedToHomePage();
    }

    /**
     * preconditions
     * <ul>
     * <li>entry URL can be accessed anonymously</li>
     * <li>login link can be clicked</li>
     * <li>login page can be displayed</li>
     * <li>given credential is valid</li>
     * </ul>
     */
    @Test
    public void test101LoginWithUsernamePass() {
        lp = hp.clickLogInLink();
        assertNotNull("fail to reach login-page", lp);

        assertTrue(lp.hasUsernameInput());
        assertTrue(lp.getUsernameInput().isEditable());
        lp.inputAtUsernameInput(USERNAME);

        assertTrue(lp.hasUserpassInput());
        assertTrue(lp.getUserpassInput().isEditable());
        lp.inputAtUserpassInput(USERPASS);

        assertTrue(lp.hasSignInButton());
        assertTrue(lp.getSignInButton().isClickable());
        hp = lp.clickSignInButton();

        assertNotNull(hp);
        assertTrue(hp.hasCreateIssueLink());
        assertTrue(hp.getCreateIssueLink().isClickable());

    }

    /**
     * preconditions
     * <ul>
     * <li>entry URL can be accessed anonymously</li>
     * <li>login link can be clicked</li>
     * <li>login page can be displayed</li>
     * </ul>
     */
    @Test
    public void test102LoginWithGoogle() {
        lp = hp.clickLogInLink();

        assertNotNull("fail to reach login-page", lp);
        assertTrue(lp.hasSignInGoogleButton());
        assertTrue(lp.getSignInGoogleButton().isClickable());

        GoogleAuthPage gap = lp.clickSignInGoogleButton();
        assertNotNull("fail to reach google-auth-page", gap);
    }
    
    @Test
    public void test103LoginFormWithUsernameNotInEmail(){
        fail("not impl");
    }
    
    @Test
    public void test104LoginFormWithLongEmailAddr(){
        fail("not impl");
    }
    @Test
    public void test105LoginFormWithShortEmailAddr(){
        fail("not impl");
    }
    
    // a lot of positive scenarios
    
    // also with some negative scenarios
    @Test
    public void test117LoginFormWithNoPassword(){
        fail("not impl");
    }
}
