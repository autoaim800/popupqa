package com.bill.atl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

import com.bill.atl.po.LoginPage;
import com.bill.atl.po.PrivateProjectPage;

public class MyTestCase {
    public static final String ENTRY_URL = "https://jira.atlassian.com/browse/TST";
    public static final String USERNAME = "autoaim800@gmail.com";
    public static final String USERPASS = "***";

    protected WebDriver driver;

    protected PrivateProjectPage hp;
    protected LoginPage lp;

    protected WebDriver initDriver() {
        ProfilesIni profile = new ProfilesIni();
        FirefoxProfile ff = profile.getProfile("selpro");
        WebDriver driver = new FirefoxDriver(ff);
        return driver;
    }

    /**
     * proceed to home page without login
     */
    protected void proceedToHomePage() {

        driver.get(ENTRY_URL);

        hp = PrivateProjectPage.waitforInstance(driver);
        assertNotNull("fail to load tst-home-page", hp);

        assertTrue("fail to see project key", hp.hasProjectKeyText());
        assertTrue(hp.getProjectKeyText().isVisible());
        assertEquals("TST", hp.getProjectKeyText().getText());

        assertTrue("fail to see project name", hp.hasProjectNameText());
        assertTrue(hp.getProjectNameText().isVisible());
        assertEquals("A Test Project", hp.getProjectNameText().getText());

        assertTrue("fail to see login link", hp.hasLogInLink());
        assertTrue(hp.getLogInLink().isClickable());
    }

    /**
     * proceed to home page with login
     */
    protected void proceedToHomePageLoggedIn() {
        proceedToHomePage();
        lp = hp.clickLogInLink();
        assertNotNull("fail to reach login-page", lp);
        assertTrue(lp.hasUsernameInput());
        assertTrue(lp.getUsernameInput().isEditable());
        lp.inputAtUsernameInput(USERNAME);
        assertTrue(lp.hasUserpassInput());
        assertTrue(lp.getUserpassInput().isEditable());
        lp.inputAtUserpassInput(USERPASS);
        assertTrue(lp.hasSignInButton());
        assertTrue(lp.getSignInButton().isClickable());
        hp = lp.clickSignInButton();
        assertNotNull(hp);
        assertTrue(hp.hasCreateIssueLink());
        assertTrue(hp.getCreateIssueLink().isClickable());
    }

    @Before
    public void setUp() {
        driver = initDriver();
        assertNotNull("fail to obtain driver", driver);
    }

    @After
    public void tearDown() {
        if (null != driver) {
            driver.close();
            driver.quit();
            driver = null;
        }
    }

}
