package com.bill.atl.ui;

import org.openqa.selenium.WebElement;

public class MyInput extends MyElement {

    public MyInput(WebElement el) {
        super(el);
    }

    /**
     * get the input value
     * 
     * @return a string of input value
     */
    public String getValue() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * whether the input is edit able
     * 
     * @return true if the input is editable
     */
    public boolean isEditable() {
        // TODO Auto-generated method stub
        return false;
    }

}
