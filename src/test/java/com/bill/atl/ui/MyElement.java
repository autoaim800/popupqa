package com.bill.atl.ui;

import org.openqa.selenium.WebElement;

public class MyElement {

    protected WebElement webElement;

    public MyElement(WebElement el) {
        webElement = el;
    }

    public WebElement getWebElement() {
        return webElement;
    }

}
